import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink} from 'react-router-dom';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
// import Sidebar from 'react-bootstrap-sidebar';



export default function DashNavBar({setCurrentComponent}){


	return(

		<>
		<Container fluid className="d-flex sticky-top" >
			<Nav className="flex-column bg-dark vh-100 p-4">
				<h3 className="text-primary"> ADMIN PANEL </h3>
				<Nav.Link href="#allProducts" onClick={() => setCurrentComponent('ProductCatalog')}> Product Summary </Nav.Link>
				{/*<Nav.Link href="#createProduct" onClick={() => setCurrentComponent('CreateProduct')}>Add Products</Nav.Link>*/}

			</Nav>
		</Container>
      	</>


		)
}