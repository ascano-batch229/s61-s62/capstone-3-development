// Destructuring components for cleaner codebase
import {Carousel} from 'react-bootstrap';


export default function	Banner(){
	return(
	    <Carousel>
	      <Carousel.Item interval={1000}>
	        <img
	          className="d-block w-100"
	          src="https://www.researchgate.net/profile/Jean-Marie-Beaulieu/publication/288825767/figure/fig1/AS:650042314596387@1531993573969/The-image-of-the-Montmorency-forest-airborne-C-X-SAR-580-image-7-looks-800x400-pixel.png"
	          alt="First slide"
	        />
	        <Carousel.Caption>
	          <h1>First slide label</h1>
	          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item interval={500}>
	        <img
	          className="d-block w-100"
	          src="https://www.itjockies.com/wp-content/uploads/featured-lycoris-recoil-an-action-and-comedy-anime-premiering-july-2-EZBqmb-800x400.jpeg"
	          alt="Second slide"
	        />
	        <Carousel.Caption>
	          <h3>Second slide label</h3>
	          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://ahegao.b-cdn.net/wp-content/uploads/2020/04/anime-girls-with-glasses__social.jpg"
	          alt="Third slide"
	        />
	        <Carousel.Caption>
	          <h3>Third slide label</h3>
	          <p>
	            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
	          </p>
	        </Carousel.Caption>
	      </Carousel.Item>
	    </Carousel>
		)
}